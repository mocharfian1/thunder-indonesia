#!/bin/sh
#
# Copyright (C) 2016 Rizki Mufrizal <mufrizalrizki@gmail.com>
#
# Distributed under terms of the MIT license.
#
echo -n "Push Ke GIT dengan pesan : "
read pesan
echo -n "Push Ke GIT 'dev' atau 'master'? "
read type

git add --all
git commit -m "$(date) -- $pesan"
git push --all

if [[ $type == dev ]]
then
	git ftp push -u thunder_indonesia@myogir.com -p thunder ftp.myogir.com
elif [[ $type == master ]] 
then
	git ftp push -u thunder@arcomega-digitalperkasa.com -p Thunder ftp.arcomega-digitalperkasa.com
else
	echo "KEY SALAH UPDATE BELUM MASUK KE SERVER"
fi