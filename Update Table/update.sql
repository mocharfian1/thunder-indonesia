ALTER TABLE `pemesanan` ADD `nama_event` TEXT NOT NULL AFTER `add_kurir_by`;

ALTER TABLE `pos_item` ADD `is_external` INT(1) NOT NULL DEFAULT '0' AFTER `item_description`;

CREATE TABLE `thunder_db`.`tanggal_acara` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `id_pemesanan` INT NOT NULL , `tanggal_awal` DATETIME NULL DEFAULT NULL , `tanggal_akhir` DATETIME NULL DEFAULT NULL , `insert_by` INT NOT NULL , `insert_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `delete_by` INT NOT NULL , `delete_date` DATETIME NOT NULL , `is_delete` INT(1) NOT NULL DEFAULT '0' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `tanggal_acara` CHANGE `id_pemesanan` `id_pemesanan` INT(11) NULL DEFAULT NULL;