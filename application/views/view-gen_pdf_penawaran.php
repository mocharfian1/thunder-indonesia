<table border="0" width="100%">
	<tr>
		<th style="text-align: left;"><img width="150px" style="filter: drop-shadow(0 3px 8px rgb(136, 136, 136));" src="<?php echo base_url('assets/img/logo/logo.png'); ?>"></th>
		<th style="text-align: right;vertical-align: top;"><h4>PT. THUNDER PRODUCTIONS INDONESIA</h4></th>
	</tr>
</table>

<div style="width: 100%; text-align: right; font-weight: bold;">Quotation Produksi</div>
<table width="100%" id="tb_identitas">
	<tr>
		<td style="border: 2px solid black;">
			<table width="100%">
				<tr >
					<th style="text-align: left;">Kepada</th>
					<th>:</th>
					<th style="text-align: left;"><?php echo $profile->pic; ?></th>
					<th style="text-align: left;">Acara</th>
					<th>:</th>
					<th style="text-align: left;"><?php echo $profile->nama_event; ?></th>
				</tr>
				<tr >
					<th style="text-align: left;">Perusahaan</th>
					<th>:</th>
					<th style="text-align: left;"><?php echo $profile->nama_perusahaan; ?></th>
					<th style="text-align: left;">Tempat</th>
					<th>:</th>
					<th style="text-align: left;"><?php echo $profile->alamat_venue; ?></th>
				</tr>
				<tr >
					<th style="text-align: left;">Nomor</th>
					<th>:</th>
					<th style="text-align: left;"><?php echo $profile->no_pemesanan; ?></th>
					<th style="text-align: left;">Tanggal</th>
					<th>:</th>
					<th style="text-align: left;"><?php echo $profile->tgl_pemesanan; ?></th>
				</tr>
				<tr >
					<th style="text-align: left;">No. Fax</th>
					<th>:</th>
					<th style="text-align: left;"><?php echo $profile->fax; ?></th>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table width="100%" cellspacing="0" id="tb_item">
	<tr style="background-color: yellow;">
		<th class="t l b" style="width: 30px;">No.</th>
		<th class="t l b" width="70px">QTY</th>
		<th class="t l b" width="100px">UNIT</th>
		<th class="t l b" colspan="3">ITEMS OF EQUIPMENT</th>
		<th class="t l b">PRICE</th>
		<th class="t l r b">TOTAL</th>
	</tr>

	<?php if(!empty($list_item_pemesanan)){ ?>
		<?php  $no=1; foreach ($list_item_pemesanan as $key => $value) {?>
			<tr>
				<td class="l"><?php echo $no; ?></td>
				<td class="l"><?php echo $value->qty; ?></td>
				<td class="l"><?php echo $value->jenis_item; ?></td>
				<td class="l" colspan="3"><?php echo $value->item_name; ?></td>
				<td class="l"><?php echo $value->harga; ?></td>
				<td class="l r">Rp. <?php echo $value->harga_akhir; ?></td>
			</tr>
		<?php $no++; } ?>
	<?php } ?>

	<tr>
		<td colspan="5" class="t  l b" style="text-align: right;">Total Produksi&nbsp;</td>
		<td class="t l b"></td>
		<td colspan="2" class="t r l b">&nbsp;Rp </td>
	</tr>
	<tr>
		<td colspan="5" class="l" style="text-align: right;">&nbsp;</td>
		<td class="l"></td>
		<td colspan="2" class="l r">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5" class="t  l b" style="text-align: right;">Sub Total&nbsp;</td>
		<td class="t l b"></td>
		<td colspan="2" class="t r l b">&nbsp;Rp </td>
	</tr>
	<tr>
		<td class="l b">&nbsp;<br><br><br><br><br></td>
		<td class="l b" colspan="2"></td>
		<td class="l b" colspan="3"></td>
		<td class="l r b" colspan="2"></td>
	</tr>
	<tr>
		<td colspan="6" class="  l b" style="text-align: right;"></td>
		<td colspan="2" class=" r l b">&nbsp;Rp </td>
	</tr>
	<tr>
		<td colspan="3" class="  l b" style="text-align: left;">Dengan Huruf</td>
		<td colspan="5" class=" r l b" style="text-align: center;">Seratus Ribu Rupiah</td>
	</tr>
	<tr>
		<td colspan="3" class="  l" style="text-align: center;">Thunder Productions</td>
		<td colspan="5" class=" r l b" style="text-align: left;">Jadwal</td>
	</tr>
	<tr>
		<td colspan="3" rowspan="5" class="  l" style="text-align: center;">&nbsp;</td>
		<td class="l b" style="text-align: left; width: 200px;">Instalasi dan Pemasangan</td>
		<td colspan="2" class="l b" style="text-align: left;"></td>
		<td class="l b" style="text-align: left;">Jam</td>
		<td class="r l b" style="text-align: left;"></td>
	</tr>
	<tr>
		<td class="l b" style="text-align: left; width: 200px;">Gladi Resik</td>
		<td colspan="2" class="l b" style="text-align: left;"></td>
		<td class="l b" style="text-align: left;">Jam</td>
		<td class="r l b" style="text-align: left;"></td>
	</tr>
	<tr>
		<td class="l b" style="text-align: left; width: 200px;">Acara</td>
		<td colspan="2" class="l b" style="text-align: left;"></td>
		<td class="l b" style="text-align: left;">Jam</td>
		<td class=" r l b" style="text-align: left;"></td>
	</tr>
	<tr>
		<td class=" r l b" colspan="5" style="text-align: center; width: 200px;"><u><i>mohon disertakan surat loading dari venue</i></u></td>
	</tr>
	<tr>
		<td class=" r l" colspan="5" style="text-align: left; width: 200px;">
			Note:
		</td>
	</tr>
	<tr>
		<td class="l b" colspan="3" style="text-align: center; width: 200px;">
			Hwe Hwe
		</td>
		<td class=" r l" colspan="5" style="text-align: left; width: 200px;">
			* Pembayaran dilakukan 50% sebagai down payment
		</td>
	</tr>
	<tr>
		<td class="l" colspan="3" style="text-align: center; width: 200px;">
			Menyetujui,
		</td>
		<td class=" r l" colspan="5" style="text-align: left; width: 200px;">
			* Pelunasan dilakukan setelah selesai setting alat
		</td>
	</tr>
	<tr>
		<td class="l" colspan="3" rowspan="4" style="text-align: center; width: 200px;">
			&nbsp;
		</td>
		<td class=" r l" colspan="5" style="text-align: left; width: 200px;">
			* Biaya di luar pajak-pajak yang dikenakan
		</td>
	</tr>
	<tr>
		<td class=" r l" colspan="5" style="text-align: left; width: 200px;">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class=" r l b" colspan="5" style="text-align: left; width: 200px;">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class=" r l" colspan="5" style="text-align: left; width: 200px;">
			Masa Berlaku
		</td>
	</tr>
	<tr>
		<td class="l b" colspan="3" style="text-align: center; width: 200px;">
			(Please Sign Here)
		</td>
		<td class=" r l b" colspan="5" style="text-align: left; width: 200px;">
			Penawaran ini berlaku sampai dengan tanggal :
		</td>
	</tr>
	
</table>

<style type="text/css">
	.t{
		border-top: 2px solid black;
	}
	.b{
		border-bottom: 2px solid black;
	}
	.r{
		border-right: 2px solid black;
	}
	.l{
		border-left: 2px solid black;
	}

	table#tb_item{
		font-size: 12px;
	}

	table#tb_identitas{
		font-size: 12px;
	}
</style>