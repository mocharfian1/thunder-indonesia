<?php
	function db(){
		return new mysqli("localhost", "root", "", "thunder_db");
	}

	function select($x=null){
		$mysqli = db();

		$result = $mysqli->query($x);
		$rows = [];
		if ($result->num_rows > 0) {
			while($r = $result->fetch_assoc()) {
				array_push($rows, $r);
			}
		}
		return $rows;	
	}

	function execute($x){
		$mysqli = db();

		$result = $mysqli->query($x);
		return $result;
	}

	$qty_out = select("select
								ip.id_pemesanan,ip.id_item,ip.qty
						from
								item_pemesanan as ip
								join pemesanan as p on p.id=ip.id_pemesanan
						where 
								p.jenis='pemesanan'
								and p.loading_status=0
								and p.`status` in (1,2,3,4,5)
								and date(now()) between date(p.loading_in) and date(p.loading_out)");

	

	if(count($qty_out)>0){
		$count_data = count($qty_out);
		foreach ($qty_out as $key => $value) {
			$ch_stat = execute("UPDATE pemesanan SET loading_status=1 WHERE  id=".$value['id_pemesanan']);
			$ch_qty = execute("UPDATE pos_item SET qty=qty-".$value['qty']." WHERE id=".$value['id_item']);
			$count_data--;
		}
		if($count_data==0){
			echo "\n".date('Y-m-d H:i:s') . " --> Sukses meng-eksekusi data Loading In.";
		}
	}else{
		echo "\n".date('Y-m-d H:i:s') . " --> Data Kosong.";
	}



	//###############################################################################################################



	$qty_in = select("select
								ip.id_pemesanan,ip.id_item,ip.qty
						from
								item_pemesanan as ip
								join pemesanan as p on p.id=ip.id_pemesanan
						where 
								p.jenis='pemesanan'
								and p.loading_status=1
								and p.`status` in (1,2,3,4,5)
								and not (date(now()) between date(p.loading_in) and date(p.loading_out))");

	if(count($qty_in)>0){
		$count_data = count($qty_in);
		foreach ($qty_in as $key => $value) {
			$ch_stat = execute("UPDATE pemesanan SET loading_status=0 WHERE  id=".$value['id_pemesanan']);
			$ch_qty = execute("UPDATE pos_item SET qty=qty+".$value['qty']." WHERE id=".$value['id_item']);
			$count_data--;
		}
		if($count_data==0){
			echo "\n".date('Y-m-d H:i:s') . " --> Sukses meng-eksekusi data Loading Out.";
		}
	}else{
		echo "\n".date('Y-m-d H:i:s') . " --> Data Kosong.";
	}


?>