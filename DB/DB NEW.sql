-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pos_db.img_item
CREATE TABLE IF NOT EXISTS `img_item` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_item` int(255) NOT NULL DEFAULT '0',
  `img_name` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `img_name` (`img_name`(10))
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1 COMMENT='Gambar Item maksimal 3';

-- Dumping data for table pos_db.img_item: ~23 rows (approximately)
/*!40000 ALTER TABLE `img_item` DISABLE KEYS */;
REPLACE INTO `img_item` (`id`, `id_item`, `img_name`) VALUES
	(87, 71, 'ID71_G0.jpg'),
	(88, 76, 'ID76_G0.jpg'),
	(89, 73, 'ID73_G0.jpg'),
	(90, 73, 'ID73_G2.jpg'),
	(91, 77, 'ID77_G0.jpg'),
	(92, 77, 'ID77_G1.jpg'),
	(93, 77, 'ID77_G2.jpg'),
	(94, 78, 'ID78_G0.jpg'),
	(95, 78, 'ID78_G1.jpg'),
	(96, 78, 'ID78_G2.jpg'),
	(97, 79, 'ID79_G0.jpg'),
	(98, 80, 'ID80_G0.jpg'),
	(99, 88, 'ID88_G0.jpg'),
	(102, 81, 'ID81_G0.jpg'),
	(103, 82, 'ID82_G0.jpg'),
	(104, 82, 'ID82_G1.jpg'),
	(105, 90, 'ID90_G0.jpg'),
	(106, 90, 'ID90_G1.jpg'),
	(107, 90, 'ID90_G2.jpg'),
	(109, 91, 'ID91_G0.jpg'),
	(117, 83, 'ID83_G0.jpg'),
	(118, 92, 'ID92_G0.jpg'),
	(121, 84, 'ID84_G0.jpg');
/*!40000 ALTER TABLE `img_item` ENABLE KEYS */;

-- Dumping structure for table pos_db.item_pemesanan
CREATE TABLE IF NOT EXISTS `item_pemesanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `id_pemesanan` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `qty_masuk` int(11) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.item_pemesanan: ~6 rows (approximately)
/*!40000 ALTER TABLE `item_pemesanan` DISABLE KEYS */;
REPLACE INTO `item_pemesanan` (`id`, `id_item`, `id_pemesanan`, `qty`, `qty_masuk`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(3, 77, 1, 2, 0, 1, '2017-12-14 23:50:20', 1, '2017-12-14 23:50:36', 0, '0000-00-00 00:00:00', 0),
	(4, 81, 1, 1, 0, 1, '2017-12-14 23:50:36', 1, '2017-12-14 23:50:36', 0, '0000-00-00 00:00:00', 0),
	(5, 80, 1, 1, 0, 1, '2017-12-14 23:50:36', 1, '2017-12-14 23:50:36', 0, '0000-00-00 00:00:00', 0),
	(6, 77, 2, 2, 0, 1, '2017-12-15 00:18:40', 1, '2017-12-15 00:19:06', 0, '0000-00-00 00:00:00', 0),
	(7, 77, 3, 1, 0, 1, '2017-12-15 01:28:32', 1, '2017-12-15 01:28:32', 0, '0000-00-00 00:00:00', 0),
	(8, 77, 4, 5, 0, 1, '2017-12-15 01:28:42', 1, '2017-12-15 01:28:42', 0, '0000-00-00 00:00:00', 0),
	(9, 77, 5, 1, 0, 1, '2017-12-15 02:58:01', 1, '2017-12-15 02:58:01', 0, '0000-00-00 00:00:00', 0),
	(10, 77, 6, 5, 0, 1, '2017-12-15 11:27:44', 1, '2017-12-15 13:10:51', 0, '0000-00-00 00:00:00', 0),
	(11, 78, 6, 2, 0, 1, '2017-12-15 13:10:51', 1, '2017-12-15 13:10:51', 0, '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `item_pemesanan` ENABLE KEYS */;

-- Dumping structure for table pos_db.item_penerimaan
CREATE TABLE IF NOT EXISTS `item_penerimaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_penerimaan` int(11) NOT NULL DEFAULT '0',
  `qty_diterima` int(11) NOT NULL DEFAULT '0',
  `update_diterima` datetime NOT NULL,
  `date_diterima` datetime NOT NULL,
  `terima_by` int(11) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.item_penerimaan: ~0 rows (approximately)
/*!40000 ALTER TABLE `item_penerimaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_penerimaan` ENABLE KEYS */;

-- Dumping structure for table pos_db.item_pengajuan
CREATE TABLE IF NOT EXISTS `item_pengajuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `id_pengajuan` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `qty_masuk` int(11) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.item_pengajuan: ~29 rows (approximately)
/*!40000 ALTER TABLE `item_pengajuan` DISABLE KEYS */;
REPLACE INTO `item_pengajuan` (`id`, `id_item`, `id_pengajuan`, `qty`, `qty_masuk`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(7, 77, 4, 12, 0, 1, '2017-12-12 16:47:19', 1, '2017-12-12 16:47:19', 0, '0000-00-00 00:00:00', 0),
	(8, 78, 5, 2, 0, 1, '2017-12-12 16:49:02', 1, '2017-12-12 16:49:02', 0, '0000-00-00 00:00:00', 0),
	(9, 77, 5, 2, 0, 1, '2017-12-12 16:49:03', 1, '2017-12-12 16:49:03', 0, '0000-00-00 00:00:00', 0),
	(15, 77, 10, 1, 0, 1, '2017-12-14 08:47:34', 1, '2017-12-14 08:47:34', 0, '0000-00-00 00:00:00', 0),
	(42, 78, 36, 10000, 0, 1, '2017-12-14 23:03:44', 1, '2017-12-14 23:18:39', 0, '0000-00-00 00:00:00', 0),
	(73, 77, 58, 20, 20, 1, '2017-12-07 22:52:58', 1, '2017-12-12 11:11:14', 0, '0000-00-00 00:00:00', 0),
	(74, 71, 59, 8, 0, 1, '2017-12-07 23:07:42', 1, '2017-12-12 11:02:59', 0, '0000-00-00 00:00:00', 0),
	(75, 77, 59, 13, 13, 1, '2017-12-07 23:07:42', 1, '2017-12-12 11:10:36', 0, '0000-00-00 00:00:00', 0),
	(76, 78, 59, 3, 3, 1, '2017-12-07 23:07:42', 1, '2017-12-12 11:10:14', 0, '0000-00-00 00:00:00', 0),
	(77, 78, 60, 50, 50, 1, '2017-12-12 10:55:16', 1, '2017-12-12 11:06:05', 0, '0000-00-00 00:00:00', 0),
	(78, 71, 60, 50, 0, 1, '2017-12-12 10:55:16', 1, '2017-12-12 11:02:52', 0, '0000-00-00 00:00:00', 0),
	(79, 78, 61, 1, 0, 1, '2017-12-12 14:59:51', 1, '2017-12-12 14:59:51', 0, '0000-00-00 00:00:00', 0),
	(80, 78, 62, 1, 0, 1, '2017-12-12 15:00:35', 1, '2017-12-12 15:00:35', 0, '0000-00-00 00:00:00', 0),
	(81, 78, 63, 1, 0, 1, '2017-12-12 15:07:08', 1, '2017-12-12 15:07:08', 0, '0000-00-00 00:00:00', 0),
	(82, 78, 64, 1, 0, 1, '2017-12-12 15:07:26', 1, '2017-12-12 15:07:26', 0, '0000-00-00 00:00:00', 0),
	(83, 78, 67, 1, 0, 1, '2017-12-12 16:31:21', 1, '2017-12-12 16:31:21', 0, '0000-00-00 00:00:00', 0),
	(84, 78, 68, 1, 0, 1, '2017-12-12 16:31:54', 1, '2017-12-12 16:31:54', 0, '0000-00-00 00:00:00', 0),
	(85, 78, 1, 1, 0, 1, '2017-12-12 16:50:44', 1, '2017-12-12 16:50:44', 0, '0000-00-00 00:00:00', 0),
	(86, 78, 10, 2121, 0, 1, '2017-12-14 08:47:34', 1, '2017-12-14 08:47:34', 0, '0000-00-00 00:00:00', 0),
	(87, 78, 10, 12, 0, 1, '2017-12-14 11:39:32', 1, '2017-12-14 11:39:32', 0, '0000-00-00 00:00:00', 0),
	(88, 77, 36, 1, 0, 1, '2017-12-14 23:03:44', 1, '2017-12-14 23:03:44', 0, '0000-00-00 00:00:00', 0),
	(89, 77, 36, 1, 0, 1, '2017-12-14 23:06:38', 1, '2017-12-14 23:06:38', 0, '0000-00-00 00:00:00', 0),
	(90, 77, 36, 1, 0, 1, '2017-12-14 23:07:31', 1, '2017-12-14 23:07:31', 0, '0000-00-00 00:00:00', 0),
	(91, 77, 36, 1, 0, 1, '2017-12-14 23:09:14', 1, '2017-12-14 23:09:14', 0, '0000-00-00 00:00:00', 0),
	(92, 77, 36, 1, 0, 1, '2017-12-14 23:09:37', 1, '2017-12-14 23:09:37', 0, '0000-00-00 00:00:00', 0),
	(93, 77, 36, 2, 0, 1, '2017-12-14 23:12:24', 1, '2017-12-14 23:12:24', 0, '0000-00-00 00:00:00', 0),
	(94, 77, 36, 2, 0, 1, '2017-12-14 23:13:20', 1, '2017-12-14 23:13:20', 0, '0000-00-00 00:00:00', 0),
	(95, 77, 36, 2, 0, 1, '2017-12-14 23:13:28', 1, '2017-12-14 23:13:28', 0, '0000-00-00 00:00:00', 0),
	(96, 77, 36, 1, 0, 1, '2017-12-14 23:14:29', 1, '2017-12-14 23:14:29', 0, '0000-00-00 00:00:00', 0),
	(97, 77, 36, 1, 0, 1, '2017-12-14 23:15:29', 1, '2017-12-14 23:15:29', 0, '0000-00-00 00:00:00', 0),
	(98, 77, 36, 1, 0, 1, '2017-12-14 23:16:46', 1, '2017-12-14 23:16:46', 0, '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `item_pengajuan` ENABLE KEYS */;

-- Dumping structure for table pos_db.pemesanan
CREATE TABLE IF NOT EXISTS `pemesanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pemesan` int(11) DEFAULT NULL,
  `id_kurir` int(11) DEFAULT NULL,
  `add_kurir_date` datetime DEFAULT NULL,
  `add_kurir_by` int(11) DEFAULT NULL,
  `no_pemesanan` text NOT NULL,
  `tgl_pemesanan` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pemesanan: ~4 rows (approximately)
/*!40000 ALTER TABLE `pemesanan` DISABLE KEYS */;
REPLACE INTO `pemesanan` (`id`, `id_pemesan`, `id_kurir`, `add_kurir_date`, `add_kurir_by`, `no_pemesanan`, `tgl_pemesanan`, `status`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(2, 44, 41, '2017-12-15 02:36:42', 1, 'PSN-1513271912-2017', '2017-12-15 00:18:40', 2, 1, '2017-12-15 00:18:40', 1, '2017-12-15 08:36:42', 0, '0000-00-00 00:00:00', 0),
	(3, 44, 41, '2017-12-15 02:37:30', 1, 'PSN-1513276104-2017', '2017-12-15 01:28:32', 2, 1, '2017-12-15 01:28:32', 1, '2017-12-15 08:37:30', 0, '0000-00-00 00:00:00', 0),
	(4, 45, 41, '2017-12-15 02:55:48', 1, 'PSN-1513276115-2017', '2017-12-15 01:28:42', 2, 1, '2017-12-15 01:28:42', 1, '2017-12-15 08:55:48', 0, '0000-00-00 00:00:00', 0),
	(5, 4, 42, '2017-12-15 02:28:25', 1, 'PSN-1513281478-2017', '2017-12-15 02:58:01', 5, 1, '2017-12-15 02:58:01', 1, '2017-12-15 08:57:21', 0, '0000-00-00 00:00:00', 0),
	(6, 45, NULL, NULL, NULL, 'PSN-1513312054-2017', '2017-12-15 11:27:44', 1, 1, '2017-12-15 11:27:44', 1, '2017-12-15 11:28:25', 0, '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `pemesanan` ENABLE KEYS */;

-- Dumping structure for table pos_db.penerimaan
CREATE TABLE IF NOT EXISTS `penerimaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `insert_by` int(11) DEFAULT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) DEFAULT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.penerimaan: ~1 rows (approximately)
/*!40000 ALTER TABLE `penerimaan` DISABLE KEYS */;
REPLACE INTO `penerimaan` (`id`, `id_pengajuan`, `status`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(1, 43, 1, NULL, '2017-12-07 10:24:48', NULL, '2017-12-07 09:04:23', NULL, '2017-12-07 09:04:23', 0),
	(3, 54, 0, 2, '2017-12-07 10:30:21', 2, '2017-12-07 10:30:21', NULL, NULL, 0);
/*!40000 ALTER TABLE `penerimaan` ENABLE KEYS */;

-- Dumping structure for table pos_db.pengajuan
CREATE TABLE IF NOT EXISTS `pengajuan` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `no_pengajuan` text NOT NULL,
  `judul` text NOT NULL,
  `tgl_pengajuan` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) DEFAULT NULL,
  `stat_penerimaan` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1 COMMENT='Table Pengajuan';

-- Dumping data for table pos_db.pengajuan: ~11 rows (approximately)
/*!40000 ALTER TABLE `pengajuan` DISABLE KEYS */;
REPLACE INTO `pengajuan` (`id`, `no_pengajuan`, `judul`, `tgl_pengajuan`, `status`, `approval`, `stat_penerimaan`, `approve_date`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(58, '1512661968', 'AAA', '2017-12-07 22:52:58', 1, 2, 1, '2017-12-07 16:53:38', 1, '2017-12-07 22:52:58', 1, '2017-12-12 11:11:14', 0, '0000-00-00 00:00:00', 0),
	(59, '1512662776', 'tes ajukan', '2017-12-07 23:07:42', 1, 2, 0, '2017-12-07 17:08:37', 1, '2017-12-07 23:07:42', 1, '2017-12-12 11:02:39', 0, '0000-00-00 00:00:00', 0),
	(60, '1513050890', 'ALAT', '2017-12-12 10:55:15', 1, 32, 0, '2017-12-12 04:56:28', 1, '2017-12-12 10:55:15', 1, '2017-12-12 11:02:37', 0, '0000-00-00 00:00:00', 0),
	(61, '1513065585', 'asdasdasd', '2017-12-12 14:59:51', 0, NULL, NULL, NULL, 1, '2017-12-12 14:59:51', 1, '2017-12-12 16:31:41', 0, '0000-00-00 00:00:00', 1),
	(62, '1513065629', 'asdfgh', '2017-12-12 15:00:35', 0, NULL, NULL, NULL, 1, '2017-12-12 15:00:35', 1, '2017-12-12 16:31:38', 0, '0000-00-00 00:00:00', 1),
	(63, '1513066019', '1', '2017-12-12 15:07:08', 0, NULL, NULL, NULL, 1, '2017-12-12 15:07:08', 1, '2017-12-12 16:31:35', 0, '0000-00-00 00:00:00', 1),
	(64, '1513066042', '2', '2017-12-12 15:07:26', 0, NULL, NULL, NULL, 1, '2017-12-12 15:07:26', 1, '2017-12-12 16:31:33', 0, '0000-00-00 00:00:00', 1),
	(65, 'PSN-1513067484-2017', '', '2017-12-12 15:31:37', 0, NULL, NULL, NULL, 1, '2017-12-12 15:31:37', 1, '2017-12-12 16:31:31', 0, '0000-00-00 00:00:00', 1),
	(66, 'PSN-1513067518-2017', '', '2017-12-12 15:32:06', 0, NULL, NULL, NULL, 1, '2017-12-12 15:32:06', 1, '2017-12-12 16:31:29', 0, '0000-00-00 00:00:00', 1),
	(67, '1513071071', 'AAA', '2017-12-12 16:31:21', 0, NULL, NULL, NULL, 1, '2017-12-12 16:31:21', 1, '2017-12-12 16:31:21', 0, '0000-00-00 00:00:00', 0),
	(68, '1513071107', 'ASSSS', '2017-12-12 16:31:54', 0, NULL, NULL, NULL, 1, '2017-12-12 16:31:54', 1, '2017-12-12 16:31:54', 0, '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `pengajuan` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_detail
CREATE TABLE IF NOT EXISTS `pos_detail` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_pos_parent` int(11) NOT NULL,
  `id_customer` int(255) NOT NULL,
  `pos_type` text NOT NULL,
  `id_item` decimal(65,0) NOT NULL,
  `jml` decimal(65,0) NOT NULL,
  `disc` decimal(65,0) NOT NULL,
  `update_by` int(11) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `delete_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remark` text NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `pos_detail` DISABLE KEYS */;
REPLACE INTO `pos_detail` (`id`, `id_pos_parent`, `id_customer`, `pos_type`, `id_item`, `jml`, `disc`, `update_by`, `insert_by`, `delete_by`, `update_date`, `insert_date`, `delete_date`, `remark`, `is_delete`) VALUES
	(1, 1, 0, '', 1, 1, 0, 0, 1, 0, '2017-11-28 11:20:36', '2017-11-28 11:20:36', '2017-11-28 11:20:36', '', 0);
/*!40000 ALTER TABLE `pos_detail` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_item
CREATE TABLE IF NOT EXISTS `pos_item` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pos_type` text NOT NULL,
  `barcode` text,
  `id_sub_kategori` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `item_name` text NOT NULL,
  `harga_beli` decimal(65,0) NOT NULL DEFAULT '0',
  `harga_jual` decimal(65,0) NOT NULL DEFAULT '0',
  `cost_percentage` float(65,2) NOT NULL DEFAULT '0.00',
  `qty` int(11) NOT NULL DEFAULT '0',
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_item: ~15 rows (approximately)
/*!40000 ALTER TABLE `pos_item` DISABLE KEYS */;
REPLACE INTO `pos_item` (`id`, `pos_type`, `barcode`, `id_sub_kategori`, `id_kategori`, `item_name`, `harga_beli`, `harga_jual`, `cost_percentage`, `qty`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(71, 'KANTOR', '8992761139018', 16, 93, 'ADES 600 ML', 2500, 5000, 50.00, 0, 1, '2017-12-05 14:29:07', 1, '2017-12-12 11:05:15', NULL, NULL, 0),
	(77, 'KANTOR', '23456788534', 17, 94, 'F4 SInar Dunia', 0, 0, 0.00, 33, 1, '2017-12-06 16:56:16', 1, '2017-12-12 11:11:14', NULL, NULL, 0),
	(78, 'KANTOR', '64563282', 15, 92, '36B Pencil', 0, 0, 0.00, 531, 1, '2017-12-06 16:57:14', 1, '2017-12-14 18:35:56', NULL, NULL, 0),
	(79, 'KANTOR', '2343244444', 15, 92, 'sdasdad', 0, 0, 0.00, 28, 1, '2017-12-14 11:23:28', 1, '2017-12-14 11:34:25', 1, '2017-12-14 05:34:25', 1),
	(80, 'KANTOR', '4343', 15, 92, 'ewrewr', 0, 0, 0.00, 23, 1, '2017-12-14 12:15:19', 1, '2017-12-14 12:15:19', NULL, NULL, 0),
	(81, 'KANTOR', '123123', 15, 92, '123123', 0, 0, 0.00, 2323, 1, '2017-12-14 17:29:57', 1, '2017-12-14 17:29:57', NULL, NULL, 0),
	(82, 'KANTOR', '43431', 15, 92, 'eewrewr', 0, 0, 0.00, 3, 1, '2017-12-14 18:36:40', 1, '2017-12-14 18:36:40', NULL, NULL, 0),
	(83, 'KANTOR', '12345678', 15, 92, 'ghfgh', 0, 0, 0.00, 5, 1, '2017-12-14 18:39:10', 1, '2017-12-14 18:39:10', NULL, NULL, 0),
	(84, 'KANTOR', '1111111111111', 15, 92, 'asd', 0, 0, 0.00, 3, 1, '2017-12-14 18:39:54', 1, '2017-12-14 18:39:54', NULL, NULL, 0),
	(85, 'KANTOR', '1', 15, 92, 'qwe', 0, 0, 0.00, 2, 1, '2017-12-14 18:42:00', 1, '2017-12-14 18:42:00', NULL, NULL, 0),
	(86, 'KANTOR', '3', 15, 92, 'wadsa', 0, 0, 0.00, 2, 1, '2017-12-14 18:45:34', 1, '2017-12-14 18:45:34', NULL, NULL, 0),
	(87, 'KANTOR', '6', 15, 92, 'dasd', 0, 0, 0.00, 1, 1, '2017-12-14 18:45:58', 1, '2017-12-14 18:45:58', NULL, NULL, 0),
	(88, 'KANTOR', '5', 15, 92, 'df', 0, 0, 0.00, 4, 1, '2017-12-14 18:47:44', 1, '2017-12-14 18:47:44', NULL, NULL, 0),
	(89, 'KANTOR', '7', 15, 92, 'fg', 0, 0, 0.00, 1, 1, '2017-12-14 18:48:44', 1, '2017-12-14 18:48:44', NULL, NULL, 0),
	(90, 'KANTOR', '1231231235', 15, 92, 'asdasd', 0, 0, 0.00, 21, 1, '2017-12-15 09:29:38', 1, '2017-12-15 09:29:38', NULL, NULL, 0),
	(91, 'KANTOR', '87654', 15, 92, 'dfsdf', 0, 0, 0.00, 2, 1, '2017-12-15 09:38:58', 1, '2017-12-15 09:38:58', NULL, NULL, 0),
	(92, 'KANTOR', '8765', 15, 92, 'rtert', 0, 0, 0.00, 5, 1, '2017-12-15 09:59:42', 1, '2017-12-15 09:59:42', NULL, NULL, 0);
/*!40000 ALTER TABLE `pos_item` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_kategori
CREATE TABLE IF NOT EXISTS `pos_kategori` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pos_type` tinytext NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` text NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_kategori: ~2 rows (approximately)
/*!40000 ALTER TABLE `pos_kategori` DISABLE KEYS */;
REPLACE INTO `pos_kategori` (`id`, `pos_type`, `code`, `description`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(89, 'KANTOR', 'BL', 'BALLPOINT', 1, '2017-11-29 17:50:36', 1, '2017-12-06 16:54:33', 1, '2017-12-06 10:54:33', 1),
	(90, 'KANTOR', 'KR', 'KERTAS', 1, '2017-11-29 17:50:49', 1, '2017-12-06 16:54:38', 1, '2017-12-06 10:54:38', 1),
	(91, 'KANTOR', 'AA', 'AAA', 1, '2017-11-30 08:38:35', 1, '2017-11-30 08:43:37', 1, '2017-11-30 02:43:37', 1),
	(92, 'KANTOR', 'PC', 'PENCIL', 1, '2017-12-04 14:57:39', 1, '2017-12-04 14:57:39', NULL, NULL, 0),
	(93, 'KANTOR', 'AIR', 'AIR MINERAL', 1, '2017-12-05 14:27:20', 1, '2017-12-05 14:27:27', NULL, NULL, 0),
	(94, 'KANTOR', 'KRT', 'KERTAS', 1, '2017-12-06 16:54:59', 1, '2017-12-06 16:54:59', NULL, NULL, 0),
	(95, 'KANTOR', 'PP', 'Power Point', 11, '2017-12-11 19:22:01', 11, '2017-12-11 19:22:01', NULL, NULL, 0);
/*!40000 ALTER TABLE `pos_kategori` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_parent
CREATE TABLE IF NOT EXISTS `pos_parent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pos_type` text,
  `bill_number` text NOT NULL,
  `date` datetime NOT NULL,
  `payment_status` int(11) NOT NULL,
  `status_bill` int(11) NOT NULL,
  `paid` decimal(65,0) NOT NULL,
  `paid_with` text NOT NULL,
  `payment_type` text,
  `id_checkin_detail` text,
  `remark_payment` text,
  `remark_item` text,
  `remark_delete` text,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` text NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` text NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_parent: ~0 rows (approximately)
/*!40000 ALTER TABLE `pos_parent` DISABLE KEYS */;
REPLACE INTO `pos_parent` (`id`, `pos_type`, `bill_number`, `date`, `payment_status`, `status_bill`, `paid`, `paid_with`, `payment_type`, `id_checkin_detail`, `remark_payment`, `remark_item`, `remark_delete`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(1, 'RESTO', '1511842799686', '2017-11-28 11:20:36', 0, 1, 5000, 'ROOM', '0', '31', 'SUDAH BAYAR BRO', 'INI REMARK ITEM', NULL, 1, '2017-11-28 11:20:36', '1', '2017-11-28 11:20:36', '', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `pos_parent` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_sub_kategori
CREATE TABLE IF NOT EXISTS `pos_sub_kategori` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pos_type` text NOT NULL,
  `sub_kategori_code` text NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `sub_description` text NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_sub_kategori: ~6 rows (approximately)
/*!40000 ALTER TABLE `pos_sub_kategori` DISABLE KEYS */;
REPLACE INTO `pos_sub_kategori` (`id`, `pos_type`, `sub_kategori_code`, `id_kategori`, `sub_description`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(10, 'KANTOR', 'BX', 89, 'BOXY', 1, '2017-11-29 17:52:19', 1, '2017-11-29 17:52:19', NULL, NULL, 0),
	(11, 'KANTOR', 'PL', 89, 'PILOT', 1, '2017-11-29 17:52:33', 1, '2017-11-29 17:52:33', NULL, NULL, 0),
	(12, 'KANTOR', 'HVS', 90, 'HVS', 1, '2017-11-29 17:52:56', 1, '2017-11-29 17:52:56', NULL, NULL, 0),
	(13, 'KANTOR', 'ST', 89, 'STANDART', 1, '2017-11-29 17:53:05', 1, '2017-11-29 18:20:19', NULL, NULL, 0),
	(14, 'KANTOR', 'BR', 90, 'BURAM', 1, '2017-12-04 14:55:16', 1, '2017-12-04 14:55:16', NULL, NULL, 0),
	(15, 'KANTOR', '2B', 92, 'PENCIL 2B', 1, '2017-12-04 14:58:03', 1, '2017-12-04 14:58:03', NULL, NULL, 0),
	(16, 'KANTOR', 'BTL', 93, 'AIR BOTOL', 1, '2017-12-05 14:27:50', 1, '2017-12-05 14:27:50', NULL, NULL, 0),
	(17, 'KANTOR', 'F4', 94, 'Folio', 1, '2017-12-06 16:55:20', 1, '2017-12-06 16:55:20', NULL, NULL, 0);
/*!40000 ALTER TABLE `pos_sub_kategori` ENABLE KEYS */;

-- Dumping structure for table pos_db.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `name` text,
  `user_type` varchar(50) NOT NULL,
  `no_pegawai` text NOT NULL,
  `department` char(50) NOT NULL,
  `jabatan` char(50) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `active_by` int(11) DEFAULT NULL,
  `active_date` datetime DEFAULT NULL,
  `insert_by` int(255) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(255) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(255) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.user: ~8 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `username`, `password`, `name`, `user_type`, `no_pegawai`, `department`, `jabatan`, `is_active`, `active_by`, `active_date`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(1, 'ykdigital', 'a11c39bb25b12b465a029e6a2d493062831b520c', 'MAS YOGI', 'Super Admin', '', '', '', 1, 1, '2017-12-15 03:44:24', 1, '0000-00-00 00:00:00', 1, '2017-12-15 09:44:24', NULL, NULL, '0'),
	(2, 'app', 'a11c39bb25b12b465a029e6a2d493062831b520c', 'MAS APPROVAL', 'Approval', '', '', '', 1, NULL, NULL, 1, '0000-00-00 00:00:00', 2, '2017-12-12 11:30:30', 1, '2017-12-12 02:08:07', '0'),
	(3, 'kurir', 'a11c39bb25b12b465a029e6a2d493062831b520c', 'MAS KURIR', 'Kurir', '', '', '', 1, NULL, NULL, 1, '0000-00-00 00:00:00', 2, '2017-12-12 11:33:55', 1, '2017-12-12 02:08:07', '0'),
	(4, 'staff', 'a11c39bb25b12b465a029e6a2d493062831b520c', 'MAS STAFF', 'Karyawan', '', '', '', 1, NULL, NULL, 1, '0000-00-00 00:00:00', 2, '2017-12-15 01:19:33', 1, '2017-12-12 02:08:07', '0'),
	(41, 'pj', 'a751c105d890f974f6f33215a06f2b4b65439388', 'PAIJO', 'Kurir', '98765', 'Pengiriman', 'Kurir', 1, 1, '2017-12-13 14:06:43', 1, '2017-12-13 06:42:32', 1, '2017-12-13 20:06:43', NULL, NULL, '0'),
	(42, 'thekurir', 'a11c39bb25b12b465a029e6a2d493062831b520c', 'Mas Paijo', 'Kurir', '889929872348', 'LJLKJASD', 'KHKJ', 1, 1, '2017-12-13 14:06:48', 1, '2017-12-13 20:06:25', 1, '2017-12-13 20:06:48', NULL, NULL, '0'),
	(43, 'staff1', '8e5d3e5b037dc159f6fc3c86585b4641f5d14abf', 'Staff 1', 'Karyawan', '99009092', 'DEPART', 'JABATAN', 1, 1, '2017-12-14 12:56:13', 1, '2017-12-14 18:54:14', 1, '2017-12-15 01:19:32', NULL, NULL, '0'),
	(44, 'staff2', '7b8dbcebfec192b79d8c5fd160737878a47450f1', 'STAFF 2', 'Karyawan', '9929299', 'DAAA', 'DEEE', 1, 1, '2017-12-14 12:56:20', 1, '2017-12-14 18:55:03', 1, '2017-12-15 01:19:30', NULL, NULL, '0'),
	(45, 'staff4', '7a38b0b3a8e0bc06a0dfb53e05f825624e22ec22', 'MAS STAFF 4', 'Karyawan', '4567890', 'DEPART', 'JBTN', 1, 1, '2017-12-14 12:56:26', 1, '2017-12-14 18:55:49', 1, '2017-12-15 01:19:29', NULL, NULL, '0');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
