-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pos_db
CREATE DATABASE IF NOT EXISTS `pos_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pos_db`;

-- Dumping structure for table pos_db.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text NOT NULL,
  `no_pegawai` text NOT NULL,
  `department` char(50) NOT NULL,
  `jabatan` char(50) NOT NULL,
  `role` char(50) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.customer: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table pos_db.img_item
CREATE TABLE IF NOT EXISTS `img_item` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_item` int(255) NOT NULL DEFAULT '0',
  `img_name` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `img_name` (`img_name`(10))
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1 COMMENT='Gambar Item maksimal 3';

-- Dumping data for table pos_db.img_item: ~4 rows (approximately)
/*!40000 ALTER TABLE `img_item` DISABLE KEYS */;
INSERT INTO `img_item` (`id`, `id_item`, `img_name`) VALUES
	(87, 71, 'ID71_G0.jpg'),
	(88, 76, 'ID76_G0.jpg'),
	(89, 73, 'ID73_G0.jpg'),
	(90, 73, 'ID73_G2.jpg');
/*!40000 ALTER TABLE `img_item` ENABLE KEYS */;

-- Dumping structure for table pos_db.item_pengajuan
CREATE TABLE IF NOT EXISTS `item_pengajuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `id_pengajuan` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.item_pengajuan: ~5 rows (approximately)
/*!40000 ALTER TABLE `item_pengajuan` DISABLE KEYS */;
INSERT INTO `item_pengajuan` (`id`, `id_item`, `id_pengajuan`, `qty`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(38, 72, 43, 6, 1, '2017-12-06 11:59:39', 1, '2017-12-06 12:21:26', 0, '0000-00-00 00:00:00', 1),
	(39, 73, 43, 1, 1, '2017-12-06 11:59:39', 1, '2017-12-06 12:21:19', 0, '0000-00-00 00:00:00', 1),
	(40, 74, 43, 1, 1, '2017-12-06 11:59:39', 1, '2017-12-06 12:00:02', 0, '0000-00-00 00:00:00', 1),
	(41, 1, 0, 2, 1, '2017-12-06 12:09:04', 1, '2017-12-06 12:11:54', 0, '0000-00-00 00:00:00', 0),
	(42, 72, 0, 13, 1, '2017-12-06 12:22:20', 1, '2017-12-06 12:22:20', 0, '0000-00-00 00:00:00', 0),
	(43, 72, 43, 16, 1, '2017-12-06 12:23:20', 1, '2017-12-06 12:23:34', 0, '0000-00-00 00:00:00', 0),
	(44, 71, 43, 5, 1, '2017-12-06 12:23:45', 1, '2017-12-06 12:23:45', 0, '0000-00-00 00:00:00', 0),
	(45, 72, 44, 1, 1, '2017-12-06 13:04:28', 1, '2017-12-06 13:04:28', 0, '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `item_pengajuan` ENABLE KEYS */;

-- Dumping structure for table pos_db.pengajuan
CREATE TABLE IF NOT EXISTS `pengajuan` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `no_pengajuan` text NOT NULL,
  `judul` text NOT NULL,
  `tgl_pengajuan` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1 COMMENT='Table Pengajuan';

-- Dumping data for table pos_db.pengajuan: ~8 rows (approximately)
/*!40000 ALTER TABLE `pengajuan` DISABLE KEYS */;
INSERT INTO `pengajuan` (`id`, `no_pengajuan`, `judul`, `tgl_pengajuan`, `status`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(43, '1512536330', 'BARUS', '2017-12-06 11:59:39', 0, 1, '2017-12-06 11:59:39', 1, '2017-12-06 12:24:07', 0, '0000-00-00 00:00:00', 0),
	(44, '1512537887', '2', '2017-12-06 13:04:28', 0, 1, '2017-12-06 13:04:28', 1, '2017-12-06 13:04:28', 0, '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `pengajuan` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_detail
CREATE TABLE IF NOT EXISTS `pos_detail` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_pos_parent` int(11) NOT NULL,
  `id_customer` int(255) NOT NULL,
  `pos_type` text NOT NULL,
  `id_item` decimal(65,0) NOT NULL,
  `jml` decimal(65,0) NOT NULL,
  `disc` decimal(65,0) NOT NULL,
  `update_by` int(11) NOT NULL,
  `insert_by` int(11) NOT NULL,
  `delete_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remark` text NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `pos_detail` DISABLE KEYS */;
INSERT INTO `pos_detail` (`id`, `id_pos_parent`, `id_customer`, `pos_type`, `id_item`, `jml`, `disc`, `update_by`, `insert_by`, `delete_by`, `update_date`, `insert_date`, `delete_date`, `remark`, `is_delete`) VALUES
	(1, 1, 0, '', 1, 1, 0, 0, 1, 0, '2017-11-28 11:20:36', '2017-11-28 11:20:36', '2017-11-28 11:20:36', '', 0);
/*!40000 ALTER TABLE `pos_detail` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_item
CREATE TABLE IF NOT EXISTS `pos_item` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pos_type` text NOT NULL,
  `barcode` text,
  `id_sub_kategori` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `item_name` text NOT NULL,
  `harga_beli` decimal(65,0) NOT NULL DEFAULT '0',
  `harga_jual` decimal(65,0) NOT NULL DEFAULT '0',
  `cost_percentage` float(65,2) NOT NULL DEFAULT '0.00',
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_item: ~6 rows (approximately)
/*!40000 ALTER TABLE `pos_item` DISABLE KEYS */;
INSERT INTO `pos_item` (`id`, `pos_type`, `barcode`, `id_sub_kategori`, `id_kategori`, `item_name`, `harga_beli`, `harga_jual`, `cost_percentage`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(71, 'KANTOR', '8992761139018', 16, 93, 'ADES 600 ML', 2500, 5000, 50.00, 1, '2017-12-05 14:29:07', 1, '2017-12-05 14:29:07', NULL, NULL, 0),
	(72, 'KANTOR', '11111', 13, 89, 'BOLPEN STANDART 0.3', 1000, 2000, 50.00, 1, '2017-12-05 14:52:28', 1, '2017-12-05 14:52:28', NULL, NULL, 0),
	(73, 'KANTOR', '677877667384', 11, 89, 'BOLPOINT PILOT', 0, 0, 0.00, 1, '2017-12-05 16:27:45', 1, '2017-12-05 16:27:45', NULL, NULL, 0),
	(74, 'KANTOR', '92839479872', 10, 89, 'BOXY SNOWMAN', 0, 0, 0.00, 1, '2017-12-05 16:28:04', 1, '2017-12-05 16:28:04', NULL, NULL, 0),
	(75, 'KANTOR', '23456', 13, 89, 'sdfsdfsdfsdf', 0, 0, 0.00, 1, '2017-12-05 16:30:45', 1, '2017-12-05 16:32:45', 1, '2017-12-05 10:32:45', 1),
	(76, 'KANTOR', '123213123', 13, 89, 'asdasdasd', 0, 0, 0.00, 1, '2017-12-05 16:31:24', 1, '2017-12-05 16:32:42', 1, '2017-12-05 10:32:42', 1);
/*!40000 ALTER TABLE `pos_item` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_kategori
CREATE TABLE IF NOT EXISTS `pos_kategori` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pos_type` tinytext NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `description` text NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_kategori: ~2 rows (approximately)
/*!40000 ALTER TABLE `pos_kategori` DISABLE KEYS */;
INSERT INTO `pos_kategori` (`id`, `pos_type`, `code`, `description`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(89, 'KANTOR', 'BL', 'BALLPOINT', 1, '2017-11-29 17:50:36', 1, '2017-11-30 12:10:09', NULL, NULL, 0),
	(90, 'KANTOR', 'KR', 'KERTAS', 1, '2017-11-29 17:50:49', 1, '2017-11-29 17:50:49', NULL, NULL, 0),
	(91, 'KANTOR', 'AA', 'AAA', 1, '2017-11-30 08:38:35', 1, '2017-11-30 08:43:37', 1, '2017-11-30 02:43:37', 1),
	(92, 'KANTOR', 'PC', 'PENCIL', 1, '2017-12-04 14:57:39', 1, '2017-12-04 14:57:39', NULL, NULL, 0),
	(93, 'KANTOR', 'AIR', 'AIR MINERAL', 1, '2017-12-05 14:27:20', 1, '2017-12-05 14:27:27', NULL, NULL, 0);
/*!40000 ALTER TABLE `pos_kategori` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_parent
CREATE TABLE IF NOT EXISTS `pos_parent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pos_type` text,
  `bill_number` text NOT NULL,
  `date` datetime NOT NULL,
  `payment_status` int(11) NOT NULL,
  `status_bill` int(11) NOT NULL,
  `paid` decimal(65,0) NOT NULL,
  `paid_with` text NOT NULL,
  `payment_type` text,
  `id_checkin_detail` text,
  `remark_payment` text,
  `remark_item` text,
  `remark_delete` text,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` text NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` text NOT NULL,
  `delete_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_parent: ~0 rows (approximately)
/*!40000 ALTER TABLE `pos_parent` DISABLE KEYS */;
INSERT INTO `pos_parent` (`id`, `pos_type`, `bill_number`, `date`, `payment_status`, `status_bill`, `paid`, `paid_with`, `payment_type`, `id_checkin_detail`, `remark_payment`, `remark_item`, `remark_delete`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(1, 'RESTO', '1511842799686', '2017-11-28 11:20:36', 0, 1, 5000, 'ROOM', '0', '31', 'SUDAH BAYAR BRO', 'INI REMARK ITEM', NULL, 1, '2017-11-28 11:20:36', '1', '2017-11-28 11:20:36', '', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `pos_parent` ENABLE KEYS */;

-- Dumping structure for table pos_db.pos_sub_kategori
CREATE TABLE IF NOT EXISTS `pos_sub_kategori` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pos_type` text NOT NULL,
  `sub_kategori_code` text NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `sub_description` text NOT NULL,
  `insert_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_by` int(11) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.pos_sub_kategori: ~6 rows (approximately)
/*!40000 ALTER TABLE `pos_sub_kategori` DISABLE KEYS */;
INSERT INTO `pos_sub_kategori` (`id`, `pos_type`, `sub_kategori_code`, `id_kategori`, `sub_description`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(10, 'KANTOR', 'BX', 89, 'BOXY', 1, '2017-11-29 17:52:19', 1, '2017-11-29 17:52:19', NULL, NULL, 0),
	(11, 'KANTOR', 'PL', 89, 'PILOT', 1, '2017-11-29 17:52:33', 1, '2017-11-29 17:52:33', NULL, NULL, 0),
	(12, 'KANTOR', 'HVS', 90, 'HVS', 1, '2017-11-29 17:52:56', 1, '2017-11-29 17:52:56', NULL, NULL, 0),
	(13, 'KANTOR', 'ST', 89, 'STANDART', 1, '2017-11-29 17:53:05', 1, '2017-11-29 18:20:19', NULL, NULL, 0),
	(14, 'KANTOR', 'BR', 90, 'BURAM', 1, '2017-12-04 14:55:16', 1, '2017-12-04 14:55:16', NULL, NULL, 0),
	(15, 'KANTOR', '2B', 92, 'PENCIL 2B', 1, '2017-12-04 14:58:03', 1, '2017-12-04 14:58:03', NULL, NULL, 0),
	(16, 'KANTOR', 'BTL', 93, 'AIR BOTOL', 1, '2017-12-05 14:27:50', 1, '2017-12-05 14:27:50', NULL, NULL, 0);
/*!40000 ALTER TABLE `pos_sub_kategori` ENABLE KEYS */;

-- Dumping structure for table pos_db.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `insert_by` int(255) NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_by` int(255) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` int(255) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `is_delete` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_db.user: ~1 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `user_type`, `insert_by`, `insert_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `is_delete`) VALUES
	(1, 'ykdigital', 'a11c39bb25b12b465a029e6a2d493062831b520c', 'Super Admin', 1, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', NULL, NULL, '0');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
